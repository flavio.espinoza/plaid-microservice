![npm (scoped)](https://img.shields.io/npm/v/plaid-microservice?label=NPM) ![NPM](https://img.shields.io/npm/l/plaid-microservice?label=License)

# plaid-microservice

A Node access layer integration with Plaid for linking U.S. bank accounts.

## Prerequisite

Signup for a Plaid account or ask your tech lead to add you to Plaid teams.

- **Signup**: <https://dashboard.plaid.com/signup>

Get your Plaid OAuth keys

- **Keys**: <https://dashboard.plaid.com/team/keys>

![](https://gitlab.com/flavio.espinoza/plaid-microservice/-/raw/master/.images/plaid-dashboard-keys.png)


Define your redirect-uri's.

- **Redirect URI**: <https://dashboard.plaid.com/team/api>

![](https://gitlab.com/flavio.espinoza/plaid-microservice/-/raw/master/.images/plaid-dashboard-api-1.png)

![](https://gitlab.com/flavio.espinoza/plaid-microservice/-/raw/master/.images/plaid-dashboard-api-2.png)

## Getting Started

Clone gitlab repo.

```shell
git clone git@gitlab.com:flavio.espinoza/plaid-microservice.git
```

CD into cloned repo.

```shell
cd plaid-microservice
```

Install dependencies using `yarn` (npm will not work).

```shell
yarn install
```

## Environment

Copy `.env.example` to an `.env` file and add your Plaid OAuth keys and redirect-uri.

```shell
cp .env.example .env
```

Add your Plaid OAuth keys and redirect-uri's to the `.env` file.

```shell
# Get your Plaid API keys from the dashboard: https://dashboard.plaid.com/account/keys
PLAID_CLIENT_ID=your_client_id
PLAID_SECRET=your_secret
# Use 'sandbox' to test with fake credentials in Plaid's Sandbox environment
# Use 'development' to test with real credentials while developing
# Use 'production' to go live with real users
PLAID_ENV=sandbox
# PLAID_PRODUCTS is a comma-separated list of products to use when
# initializing Link, e.g. PLAID_PRODUCTS=auth,transactions.
# see https://plaid.com/docs/api/tokens/#link-token-create-request-products for a complete list
# Important: When moving to Production, make sure to update this list with only the products 
# you plan to use. Otherwise, you may be billed for unneeded products.
PLAID_PRODUCTS=auth,transactions
# PLAID_COUNTRY_CODES is a comma-separated list of countries to use when
# initializing Link, e.g. PLAID_COUNTRY_CODES=US,CA.
# see https://plaid.com/docs/api/tokens/#link-token-create-request-country-codes for a complete list
PLAID_COUNTRY_CODES=US,CA
# Only required for OAuth:
# Set PLAID_REDIRECT_URI to 'http://localhost:3000/plaid-redirect'
# The OAuth redirect flow requires an endpoint on the developer's website
# that the bank website should redirect to. You will need to configure
# this redirect URI for your client ID through the Plaid developer dashboard
# at https://dashboard.plaid.com/team/api.
PLAID_REDIRECT_URI=http://localhost:3000/plaid-redirect
```

## Docker

Make sure Docker is running on your local dev environment.

```shell
docker -v
```

Build and run the Docker image with Make.

```shell
make up
```

- [ ] TODO: Add Docker Image to Docker Hub.

## CI/CD

- [ ] TODO: Add full CI/CD

```shell
stages:
  - build
  - test
  - qa
  - staging
  - deploy
```
